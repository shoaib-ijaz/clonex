# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('professionals', '0002_professional_favoriting_customers'),
    ]

    operations = [
        migrations.AlterField(
            model_name='professional',
            name='favoriting_customers',
            field=models.ManyToManyField(blank=True, to='customers.Customer', through='favorites.FavoriteProfessional', verbose_name='Favoriting Customers'),
        ),
    ]
