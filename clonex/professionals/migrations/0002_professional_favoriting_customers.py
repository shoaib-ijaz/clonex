# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0002_customer_favorited_professionals'),
        ('favorites', '0001_initial'),
        ('professionals', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='professional',
            name='favoriting_customers',
            field=models.ManyToManyField(to='customers.Customer', verbose_name='Favoriting Customers', through='favorites.FavoriteProfessional'),
        ),
    ]
