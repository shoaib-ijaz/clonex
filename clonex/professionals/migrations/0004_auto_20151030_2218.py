# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0004_auto_20151030_1909'),
        ('recommendations', '0001_initial'),
        ('professionals', '0003_auto_20151030_1909'),
    ]

    operations = [
        migrations.AddField(
            model_name='professional',
            name='recommending_customers',
            field=models.ManyToManyField(verbose_name='Recommending Customers', through='recommendations.ProfessionalRecommendation', blank=True, related_name='_recommending_customers_+', to='customers.Customer'),
        ),
        migrations.AlterField(
            model_name='professional',
            name='favoriting_customers',
            field=models.ManyToManyField(verbose_name='Favoriting Customers', through='favorites.FavoriteProfessional', blank=True, related_name='_favoriting_customers_+', to='customers.Customer'),
        ),
    ]
