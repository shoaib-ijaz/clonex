# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import geoposition.fields
from django.conf import settings
import phonenumber_field.modelfields
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('salons', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Professional',
            fields=[
                ('is_active', models.BooleanField(verbose_name='Is Active', default=True)),
                ('street', models.CharField(verbose_name='Street', max_length=128)),
                ('locality', models.CharField(verbose_name='Locality', max_length=128)),
                ('city', models.CharField(verbose_name='City', choices=[('North Cyprus', (('XXX_NICOSIA', 'Nicosia'), ('XXX_KYRENIA', 'Kyrenia'), ('XXX_FAMAGUSTA', 'Famagusta'), ('XXX_GUZELYURT', 'Guzelyurt'), ('XXX_ISKELE', 'Iskele')))], max_length=32)),
                ('country', models.CharField(verbose_name='Country', choices=[('XXX', 'North Cyprus')], max_length=3)),
                ('geoposition', geoposition.fields.GeopositionField(verbose_name='Geoposition', max_length=42)),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(verbose_name='Phone Number', blank=True, max_length=128)),
                ('website_url', models.URLField(verbose_name='Website URL', blank=True)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('user', models.OneToOneField(serialize=False, related_query_name='professional', primary_key=True, verbose_name='User', to=settings.AUTH_USER_MODEL)),
                ('slug', autoslug.fields.AutoSlugField(verbose_name='Slug', editable=False, populate_from='get_name_and_salon_and_full_address', max_length=512)),
                ('name', models.CharField(verbose_name='Name', max_length=128)),
                ('description', models.TextField(verbose_name='Description')),
                ('does_online_booking', models.BooleanField(verbose_name='Does Online Booking', default=False)),
                ('salon', models.ForeignKey(null=True, related_query_name='professional', blank=True, verbose_name='Salon', to='salons.Salon', related_name='professionals')),
            ],
            options={
                'verbose_name': 'Professional',
                'verbose_name_plural': 'Professionals',
                'ordering': ('country', 'city', 'locality'),
            },
        ),
        migrations.CreateModel(
            name='ProfessionalPicture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('picture_height', models.PositiveIntegerField(verbose_name='Picture Height', default=0)),
                ('picture_width', models.PositiveIntegerField(verbose_name='Picture Width', default=0)),
                ('picture', models.ImageField(verbose_name='Picture', height_field='picture_height', upload_to='professionals', width_field='picture_width', max_length=256)),
                ('is_primary', models.NullBooleanField()),
                ('professional', models.ForeignKey(related_query_name='professional_picture', verbose_name='Professional', to='professionals.Professional', related_name='professional_pictures')),
            ],
            options={
                'verbose_name': 'Professional Picture Relationship',
                'verbose_name_plural': 'Professional Picture Relationships',
                'ordering': ('professional', '-is_primary', '-created_on'),
            },
        ),
        migrations.CreateModel(
            name='ProfessionalWorkingDay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('working_day', models.CharField(verbose_name='Working Day', choices=[('mon', 'Monday'), ('tue', 'Tuesday'), ('wed', 'Wednesday'), ('thu', 'Thursday'), ('fri', 'Friday'), ('sat', 'Saturday'), ('sun', 'Sunday')], max_length=3)),
                ('does_morning', models.BooleanField(verbose_name='Does Morning', default=False)),
                ('does_afternoon', models.BooleanField(verbose_name='Does Afternoon', default=False)),
                ('does_evening', models.BooleanField(verbose_name='Does Evening', default=False)),
                ('does_night', models.BooleanField(verbose_name='Does Night', default=False)),
                ('professional', models.ForeignKey(related_query_name='related_working_day', verbose_name='Professional', to='professionals.Professional', related_name='related_working_days')),
            ],
            options={
                'verbose_name': 'Professional Working Day Relationship',
                'verbose_name_plural': 'Professional Working Day Relationships',
                'ordering': ('professional', 'working_day'),
            },
        ),
        migrations.CreateModel(
            name='ProfessionalWorkingPeriod',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('working_day', models.CharField(verbose_name='Working Day', choices=[('mon', 'Monday'), ('tue', 'Tuesday'), ('wed', 'Wednesday'), ('thu', 'Thursday'), ('fri', 'Friday'), ('sat', 'Saturday'), ('sun', 'Sunday')], max_length=3)),
                ('working_from', models.TimeField(verbose_name='Working From')),
                ('working_to', models.TimeField(verbose_name='Working To')),
                ('professional', models.ForeignKey(related_query_name='related_working_period', verbose_name='Professional', to='professionals.Professional', related_name='related_working_periods')),
            ],
            options={
                'verbose_name': 'Professional Working Period Relationship',
                'verbose_name_plural': 'Professional Working Period Relationships',
                'ordering': ('professional', 'working_day', 'working_from'),
            },
        ),
        migrations.AlterUniqueTogether(
            name='professionalworkingday',
            unique_together=set([('professional', 'working_day')]),
        ),
        migrations.AlterUniqueTogether(
            name='professionalpicture',
            unique_together=set([('professional', 'is_primary')]),
        ),
    ]
