from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import (
    Professional, 
    ProfessionalPicture, 
    ProfessionalWorkingDay, 
    ProfessionalWorkingPeriod,
)


class ProfessionalPictureInline(admin.TabularInline):
    model = ProfessionalPicture


class ProfessionalWorkingPeriodInline(admin.TabularInline):
    model = ProfessionalWorkingPeriod


class ProfessionalAdmin(admin.ModelAdmin):
    list_display = ('name', 'locality', 'city', 'is_active',)
    list_filter = ('city', 'country', 'is_active',)
    search_fields = ('name',)
    fieldsets = (
        (_('Basic'), {
            'fields': ('name', 'description',)
        }),
        (_('Salon'), {
            'fields': ('salon',)
        }),
        (_('Address'), {
            'fields': ('street', 'locality', 'city', 'country', 'geoposition',)
        }),
        (_('Contact Info'), {
            'fields': ('phone_number', 'website_url',)
        }),
        (_('Online Booking'), {
            'fields': ('does_online_booking',)
        }),
        (_('System'), {
            'fields': ('user', 'slug', 'created_on', 'modified_on',)
        }),
        (_('Admin'), {
            'fields': ('is_active',)
        }),
    )
    readonly_fields = ('slug', 'created_on', 'modified_on',)
    inlines = (ProfessionalPictureInline, ProfessionalWorkingPeriodInline,)


class ProfessionalPictureAdmin(admin.ModelAdmin):
    pass


class ProfessionalWorkingDayAdmin(admin.ModelAdmin):
    pass


class ProfessionalWorkingPeriodAdmin(admin.ModelAdmin):
    pass


admin.site.register(Professional, ProfessionalAdmin)
admin.site.register(ProfessionalPicture, ProfessionalPictureAdmin)
admin.site.register(ProfessionalWorkingDay, ProfessionalWorkingDayAdmin)
admin.site.register(ProfessionalWorkingPeriod, ProfessionalWorkingPeriodAdmin)