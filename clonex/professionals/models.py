from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from autoslug import AutoSlugField

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

from clonex.core.behaviors import Activable, Addressable, ContactInfoble, Timestampable
from clonex.core.choices import Days
from clonex.core.mongodb import mongodb

# from . import to_mongodb


class Professional(Activable, Addressable, ContactInfoble, Timestampable, models.Model):
    NAME_MAX_LENGTH = 128
    SLUG_MAX_LENGTH = 512

    # OneToOneField to User model
    user = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True, 
        related_query_name='professional',
        verbose_name=_("User"))

    # system # auto generated field
    slug = AutoSlugField(max_length=SLUG_MAX_LENGTH, populate_from='get_name_and_salon_and_full_address',
        verbose_name=_("Slug"))

    # Basic
    name = models.CharField(max_length=NAME_MAX_LENGTH, 
        verbose_name=_("Name"))
    description = models.TextField(
        verbose_name=_("Description"))

    # Salon
    salon = models.ForeignKey('salons.Salon', blank=True, null=True,
        related_name='professionals', 
        related_query_name='professional', 
        verbose_name=_("Salon"))

    # Booking
    does_online_booking = models.BooleanField(default=False,
        verbose_name=_("Does Online Booking"))


    # Customers
    # ---------------------------------------------------------------------
    favoriting_customers = models.ManyToManyField('customers.Customer', blank=True,
        through='favorites.FavoriteProfessional',
        related_name='+',
        verbose_name=_("Favoriting Customers"))

    recommending_customers = models.ManyToManyField('customers.Customer', blank=True,
        through='recommendations.ProfessionalRecommendation',
        related_name='+',
        verbose_name=_("Recommending Customers"))


    class Meta:
        ordering = ('country', 'city', 'locality',)
        verbose_name = _("Professional")
        verbose_name_plural = _("Professionals")


    def __str__(self):
        return _("Professional [{pk}]: {full_name}").format(
            pk=self.pk, 
            full_name=self.user.get_full_name())


    def get_absolute_url(self):
        return reverse('professionals:professional_detail', kwargs={'pk': self.pk})

    def get_full_address(self):
        return _("{street}, {locality}, {city}, {country}").format(
            street=self.street,
            locality=self.locality,
            city=self.get_city_display(),
            country=self.get_country_display())

    def get_name_and_full_address(self):
        return _("{name}, {full_address}").format(
            name=self.name,
            full_address=self.get_full_address())

    def get_name_and_salon_and_full_address(self):
        try:
            return _("{name}, {salon}, {full_address}").format(
                name=self.name,
                salon=self.salon.name,
                full_address=self.get_full_address())
        except:
            return self.get_name_and_full_address()

    def get_primary_image(self):
        ### IMPROVE HERE BY SELECTING PRIMARY IMAGE
        ### IMPLEMENT AUTO IS_PRIMARY FIELD FOR PROFESSIONAL IMAAGE
        try:
            return self.professional_images.all()[0]
        except IndexError:
            return 


# @receiver([models.signals.post_save], sender=Professional)
# def save_professional_on_mongodb(sender, instance, **kwargs):
#     mongodb.professionals.update({'id': instance.id}, {'$set': to_mongodb.professional_detail(instance)}, upsert=True)


# @receiver([models.signals.post_delete], sender=Professional)
# def delete_professional_on_mongodb(sender, instance, **kwargs):
#     mongodb.professionals.remove({'id': instance.id})


class ProfessionalPicture(Timestampable, models.Model):
    PICTURE_MAX_LENGTH = 256

    professional = models.ForeignKey('professionals.Professional', 
        related_name='professional_pictures', 
        related_query_name='professional_picture', 
        verbose_name=_("Professional"))
    picture_height = models.PositiveIntegerField(default=0, 
        verbose_name=_("Picture Height"))
    picture_width = models.PositiveIntegerField(default=0, 
        verbose_name=_("Picture Width"))
    picture = models.ImageField(max_length=PICTURE_MAX_LENGTH, height_field='picture_height', width_field='picture_width',
        upload_to='professionals', 
        verbose_name=_("Picture"))
    picture_320_320 = ImageSpecField(
        source='picture', processors=[ResizeToFill(320, 320)], format='JPEG', options={'quality': 100})
    picture_640_640 = ImageSpecField(
        source='picture', processors=[ResizeToFill(640, 640)], format='JPEG', options={'quality': 100})
    picture_1080_1080 = ImageSpecField(
        source='picture', processors=[ResizeToFill(1080, 1080)], format='JPEG', options={'quality': 100})
    is_primary = models.NullBooleanField()


    class Meta:
        ordering = ('professional', '-is_primary','-created_on',)
        unique_together = ('professional', 'is_primary',)
        verbose_name = _("Professional Picture Relationship")
        verbose_name_plural = _("Professional Picture Relationships")


    def __str__(self):
        return _("Professional Picture Relationship [{id}]: {professional} {picture}").format(
            id=self.id,
            professional=self.professional,
            picture=self.picture)


# @receiver([models.signals.post_save], sender=ProfessionalPicture)
# def save_professional_picture_on_mongodb(sender, instance, **kwargs):
#     mongodb.professional_pictures.update({'id': instance.id}, {'$set': to_mongodb.professional_picture_detail(instance)}, upsert=True)


# @receiver([models.signals.post_delete], sender=ProfessionalPicture)
# def delete_professional_picture_on_mongodb(sender, instance, **kwargs):
#     mongodb.professional_pictures.remove({'id': instance.id})



class ProfessionalWorkingDay(models.Model):
    professional = models.ForeignKey('professionals.Professional', 
        related_name='related_working_days', 
        related_query_name='related_working_day', 
        verbose_name=_("Professional"))
    working_day = models.CharField(max_length=Days.CHOICE_MAX_LENGTH, choices=Days.CHOICES,
        verbose_name=_("Working Day"))

    # Morning:  6AM-11:59AM
    does_morning = models.BooleanField(default=False,
        verbose_name=_("Does Morning")) 
    # Afternoon:12PM-3:59PM
    does_afternoon = models.BooleanField(default=False,
        verbose_name=_("Does Afternoon")) 
    # Evening:  4PM-7:59PM
    does_evening = models.BooleanField(default=False,
        verbose_name=_("Does Evening")) 
    # Night:    8PM-11:59PM
    does_night = models.BooleanField(default=False,
        verbose_name=_("Does Night"))


    class Meta:
        ordering = ('professional', 'working_day',)
        unique_together = ('professional', 'working_day',)
        verbose_name = _("Professional Working Day Relationship")
        verbose_name_plural = _("Professional Working Day Relationships")


class ProfessionalWorkingPeriod(models.Model):
    professional = models.ForeignKey('professionals.Professional', 
        related_name='related_working_periods', 
        related_query_name='related_working_period', 
        verbose_name=_("Professional"))
    working_day = models.CharField(max_length=Days.CHOICE_MAX_LENGTH, choices=Days.CHOICES,
        verbose_name=_("Working Day"))
    working_from = models.TimeField(
        verbose_name=_("Working From")) 
    working_to = models.TimeField(
        verbose_name=_("Working To"))


    class Meta:
        ordering = ('professional', 'working_day', 'working_from',)
        verbose_name = _("Professional Working Period Relationship")
        verbose_name_plural = _("Professional Working Period Relationships")