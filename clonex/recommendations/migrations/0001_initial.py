# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('salons', '0001_initial'),
        ('professionals', '0001_initial'),
        ('customers', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfessionalRecommendation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('recommendation', models.TextField(verbose_name='Recommendation')),
                ('customer', models.ForeignKey(verbose_name='Customer', to='customers.Customer')),
                ('professional', models.ForeignKey(verbose_name='Professional', to='professionals.Professional')),
            ],
            options={
                'verbose_name': 'Professional Recommendation',
                'verbose_name_plural': 'Professional Recommendations',
                'ordering': ('professional', 'customer', '-created_on'),
            },
        ),
        migrations.CreateModel(
            name='SalonRecommendation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('recommendation', models.TextField(verbose_name='Recommendation')),
                ('customer', models.ForeignKey(verbose_name='Customer', to='customers.Customer')),
                ('salon', models.ForeignKey(verbose_name='Salon', to='salons.Salon')),
            ],
            options={
                'verbose_name': 'Salon Recommendation',
                'verbose_name_plural': 'Salon Recommendations',
                'ordering': ('salon', 'customer', '-created_on'),
            },
        ),
        migrations.AlterUniqueTogether(
            name='salonrecommendation',
            unique_together=set([('customer', 'salon')]),
        ),
        migrations.AlterUniqueTogether(
            name='professionalrecommendation',
            unique_together=set([('customer', 'professional')]),
        ),
    ]
