from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from clonex.core.behaviors import Timestampable


class ProfessionalRecommendation(Timestampable, models.Model):
    customer = models.ForeignKey('customers.Customer',
        verbose_name="Customer")
    professional = models.ForeignKey('professionals.Professional', 
        verbose_name="Professional")
    recommendation = models.TextField(
        verbose_name="Recommendation")


    class Meta:
        unique_together = ('customer', 'professional')
        ordering = ('professional', 'customer', '-created_on',)
        verbose_name = _("Professional Recommendation")
        verbose_name_plural = _("Professional Recommendations")


    def __str__(self):
        return _("Professional Recommendation [{id}]: {customer} {professional}").format(
            id=self.id,
            customer=self.customer,
            professional=self.professional)

    def get_absolute_url(self):
        return reverse('recommendations:professional_recommendation_detail', kwargs={'id': self.id})


class SalonRecommendation(Timestampable, models.Model):
    customer = models.ForeignKey('customers.Customer', 
        verbose_name="Customer")
    salon = models.ForeignKey('salons.Salon', 
        verbose_name="Salon")
    recommendation = models.TextField(
        verbose_name="Recommendation")


    class Meta:
        unique_together = ('customer', 'salon')
        ordering = ('salon', 'customer', '-created_on',)
        verbose_name = _("Salon Recommendation")
        verbose_name_plural = _("Salon Recommendations")


    def __str__(self):
        return _("Salon Recommendation [{id}]: {customer} {salon}").format(
            id=self.id,
            customer=self.customer,
            salon=self.salon)

    def get_absolute_url(self):
        return reverse('recommendations:salon_recommendation_detail', kwargs={'id': self.id})