from django.contrib import admin

from .models import ProfessionalRecommendation


class ProfessionalRecommendationAdmin(admin.ModelAdmin):
    list_display = ('customer', 'professional', 'recommendation',)


admin.site.register(ProfessionalRecommendation, ProfessionalRecommendationAdmin)