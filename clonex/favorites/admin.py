from django.contrib import admin

from .models import FavoriteProfessional, FavoriteSalon


class FavoriteProfessionalAdmin(admin.ModelAdmin):
    list_display = ('customer', 'professional',)


class FavoriteSalonAdmin(admin.ModelAdmin):
    list_display = ('customer', 'salon',)


admin.site.register(FavoriteProfessional, FavoriteProfessionalAdmin)
admin.site.register(FavoriteSalon, FavoriteSalonAdmin)