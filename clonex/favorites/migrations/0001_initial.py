# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '__first__'),
        ('salons', '__first__'),
        ('professionals', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='FavoriteProfessional',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('customer', models.ForeignKey(verbose_name='Customer', to='customers.Customer')),
                ('professional', models.ForeignKey(verbose_name='Professional', to='professionals.Professional')),
            ],
            options={
                'verbose_name': 'Favorite Professional',
                'ordering': ('customer', 'professional'),
                'verbose_name_plural': 'Favorite Professionals',
            },
        ),
        migrations.CreateModel(
            name='FavoriteSalon',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('customer', models.ForeignKey(verbose_name='Customer', to='customers.Customer')),
                ('salon', models.ForeignKey(verbose_name='Salon', to='salons.Salon')),
            ],
            options={
                'verbose_name': 'Favorite Salon',
                'ordering': ('customer', 'salon'),
                'verbose_name_plural': 'Favorite Salons',
            },
        ),
        migrations.AlterUniqueTogether(
            name='favoritesalon',
            unique_together=set([('customer', 'salon')]),
        ),
        migrations.AlterUniqueTogether(
            name='favoriteprofessional',
            unique_together=set([('customer', 'professional')]),
        ),
    ]
