from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from clonex.core.behaviors import Timestampable


class FavoriteProfessional(Timestampable, models.Model):
    customer = models.ForeignKey('customers.Customer',
        verbose_name="Customer")
    professional = models.ForeignKey('professionals.Professional', 
        verbose_name="Professional")


    class Meta:
        unique_together = ('customer', 'professional')
        ordering = ('customer', 'professional')
        verbose_name = _("Favorite Professional")
        verbose_name_plural = _("Favorite Professionals")


    def __str__(self):
        return _("Favorite Professional [{id}]: {customer} {professional}").format(
            id=self.id,
            customer=self.customer,
            professional=self.professional)

    def get_absolute_url(self):
        return reverse('favorites:favorite_professional_detail', kwargs={'id': self.id})


class FavoriteSalon(Timestampable, models.Model):
    customer = models.ForeignKey('customers.Customer', 
        verbose_name="Customer")
    salon = models.ForeignKey('salons.Salon', 
        verbose_name="Salon")


    class Meta:
        unique_together = ('customer', 'salon')
        ordering = ('customer', 'salon')
        verbose_name = _("Favorite Salon")
        verbose_name_plural = _("Favorite Salons")


    def __str__(self):
        return _("Favorite Salon [{id}]: {customer} {salon}").format(
            id=self.id,
            customer=self.customer,
            salon=self.salon)

    def get_absolute_url(self):
        return reverse('favorites:favorite_salon_detail', kwargs={'id': self.id})