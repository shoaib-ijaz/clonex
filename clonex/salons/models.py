from django.core.urlresolvers import reverse
from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from autoslug import AutoSlugField

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

from clonex.core.behaviors import Activable, Addressable, ContactInfoble, Timestampable
from clonex.core.choices import Days
from clonex.core.mongodb import mongodb

# from . import to_mongodb


class Salon(Activable, Addressable, Timestampable, ContactInfoble, models.Model):
    NAME_MAX_LENGTH = 128
    SLUG_MAX_LENGTH = 512

    # basic
    name = models.CharField(max_length=NAME_MAX_LENGTH, 
        verbose_name=_("Name"))
    description = models.TextField(
        verbose_name=_("Description"))

    # system # auto generated field
    slug = AutoSlugField(max_length=SLUG_MAX_LENGTH, populate_from='get_name_and_full_address',
        verbose_name=_("Slug"))


    # Professionals
    # -----------------------------------------------------------------------
    favoriting_customers = models.ManyToManyField('customers.Customer',  blank=True,
        through='favorites.FavoriteSalon',
        related_name='+',
        verbose_name=_("Favoriting Customers"))


    recommending_customers = models.ManyToManyField('customers.Customer',  blank=True,
        through='recommendations.SalonRecommendation',
        related_name='+',
        verbose_name=_("Recommending Customers"))


    class Meta:
        ordering = ('country', 'city', 'locality', 'name',)
        verbose_name = _("Salon")
        verbose_name_plural = _("Salons")


    def __str__(self):
        return _("Salon [{id}]: {name}, {locality}, {city}").format(
            id=self.id,
            name=self.name,
            locality=self.locality,
            city=self.get_city_display())


    def get_absolute_url(self):
        return reverse('salons:salon_detail', kwargs={'slug':self.slug, 'id':self.id})

    def get_full_address(self):
        return _("{street}, {locality}, {city}, {country}").format(
            street=self.street,
            locality=self.locality,
            city=self.get_city_display(),
            country=self.get_country_display())

    def get_name_and_full_address(self):
        return _("{name}, {full_address}").format(
            name=self.name,
            full_address=self.get_full_address())


# @receiver([models.signals.post_save], sender=Salon)
# def save_salon_on_mongodb(sender, instance, **kwargs):
#     mongodb.salons.update({'id': instance.id}, {'$set': to_mongodb.salon_detail(instance)}, upsert=True)


# @receiver([models.signals.post_delete], sender=Salon)
# def delete_salon_on_mongodb(sender, instance, **kwargs):
#     mongodb.salons.remove({'id': instance.id})


class SalonPicture(Timestampable, models.Model):
    PICTURE_MAX_LENGTH = 256

    salon = models.ForeignKey('salons.Salon', 
        related_name='salon_pictures', 
        related_query_name='salon_picture', 
        verbose_name=_("Salon"))
    picture_height = models.PositiveIntegerField(default=0, 
        verbose_name=_("Picture Height"))
    picture_width = models.PositiveIntegerField(default=0, 
        verbose_name=_("Picture Width"))
    picture = models.ImageField(max_length=PICTURE_MAX_LENGTH, height_field='picture_height', width_field='picture_width',
        upload_to='salons', 
        verbose_name=_("Picture"))
    picture_320_320 = ImageSpecField(
        source='picture', processors=[ResizeToFill(320, 320)], format='JPEG', options={'quality': 100})
    picture_640_640 = ImageSpecField(
        source='picture', processors=[ResizeToFill(640, 640)], format='JPEG', options={'quality': 100})
    picture_1080_1080 = ImageSpecField(
        source='picture', processors=[ResizeToFill(1080, 1080)], format='JPEG', options={'quality': 100})
    is_primary = models.NullBooleanField()


    class Meta:
        ordering = ('salon', '-is_primary','-created_on',)
        unique_together = ('salon', 'is_primary',)
        verbose_name = _("Salon Picture Relationship")
        verbose_name_plural = _("Salon Picture Relationships")


    def __str__(self):
        return _("Salon Picture Relationship [{id}]: {salon} {picture}").format(
            id=self.id,
            salon=self.salon,
            picture=self.picture)


# @receiver([models.signals.post_save], sender=SalonPicture)
# def save_salon_picture_on_mongodb(sender, instance, **kwargs):
#     mongodb.salon_pictures.update({'id': instance.id}, {'$set': to_mongodb.salon_picture_detail(instance)}, upsert=True)


# @receiver([models.signals.post_delete], sender=SalonPicture)
# def delete_salon_picture_on_mongodb(sender, instance, **kwargs):
#     mongodb.salon_pictures.remove({'id': instance.id})



class SalonWorkingDay(models.Model):
    salon = models.ForeignKey('salons.Salon', 
        related_name='related_working_days', 
        related_query_name='related_working_day', 
        verbose_name=_("Salon"))
    working_day = models.CharField(max_length=Days.CHOICE_MAX_LENGTH, choices=Days.CHOICES,
        verbose_name=_("Working Day"))

    # Morning:  6AM-11:59AM
    does_morning = models.BooleanField(default=False,
        verbose_name=_("Does Morning")) 
    # Afternoon:12PM-3:59PM
    does_afternoon = models.BooleanField(default=False,
        verbose_name=_("Does Afternoon")) 
    # Evening:  4PM-7:59PM
    does_evening = models.BooleanField(default=False,
        verbose_name=_("Does Evening")) 
    # Night:    8PM-11:59PM
    does_night = models.BooleanField(default=False,
        verbose_name=_("Does Night"))


    class Meta:
        ordering = ('salon', 'working_day',)
        unique_together = ('salon', 'working_day',)
        verbose_name = _("Salon Working Day Relationship")
        verbose_name_plural = _("Salon Working Day Relationships")


class SalonWorkingPeriod(models.Model):
    salon = models.ForeignKey('salons.Salon', 
        related_name='related_working_periods', 
        related_query_name='related_working_period', 
        verbose_name=_("Salon"))
    working_day = models.CharField(max_length=Days.CHOICE_MAX_LENGTH, choices=Days.CHOICES,
        verbose_name=_("Working Day"))
    working_from = models.TimeField(
        verbose_name=_("Working From")) 
    working_to = models.TimeField(
        verbose_name=_("Working To")) 


    class Meta:
        ordering = ('salon', 'working_day', 'working_from',)
        verbose_name = _("Salon Working Period Relationship")
        verbose_name_plural = _("Salon Working Period Relationships")