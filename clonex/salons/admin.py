from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from clonex.professionals.models import Professional

from .models import (
    Salon,
    SalonPicture,
    SalonWorkingDay,
    SalonWorkingPeriod,
)


class SalonPictureInline(admin.TabularInline):
    model = SalonPicture


class SalonWorkingPeriodInline(admin.TabularInline):
    model = SalonWorkingPeriod


class ProfessionalInline(admin.TabularInline):
    model = Professional


class SalonAdmin(admin.ModelAdmin):
    list_display = ('name', 'locality', 'city', 'is_active',)
    list_filter = ('city', 'country', 'is_active',)
    search_fields = ('name',)
    fieldsets = (
        (_('Basic'), {
            'fields': ('name', 'description',)
        }),
        (_('Address'), {
            'fields': ('street', 'locality', 'city', 'country', 'geoposition',)
        }),
        (_('Contact Info'), {
            'fields': ('phone_number', 'website_url',)
        }),
        (_('System'), {
            'fields': ('slug', 'created_on', 'modified_on',)
        }),
        (_('Admin'), {
            'fields': ('is_active',)
        }),
    )
    readonly_fields = ('slug', 'created_on', 'modified_on',)
    inlines = (SalonPictureInline, SalonWorkingPeriodInline, ProfessionalInline)


class SalonPictureAdmin(admin.ModelAdmin):
    pass


class SalonWorkingDayAdmin(admin.ModelAdmin):
    pass


class SalonWorkingPeriodAdmin(admin.ModelAdmin):
    pass


admin.site.register(Salon, SalonAdmin)
admin.site.register(SalonPicture, SalonPictureAdmin)
admin.site.register(SalonWorkingDay, SalonWorkingDayAdmin)
admin.site.register(SalonWorkingPeriod, SalonWorkingPeriodAdmin)