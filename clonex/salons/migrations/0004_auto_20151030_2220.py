# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0005_auto_20151030_2220'),
        ('recommendations', '0001_initial'),
        ('salons', '0003_auto_20151030_1909'),
    ]

    operations = [
        migrations.AddField(
            model_name='salon',
            name='recommending_customers',
            field=models.ManyToManyField(related_name='_recommending_customers_+', through='recommendations.SalonRecommendation', verbose_name='Recommending Customers', blank=True, to='customers.Customer'),
        ),
        migrations.AlterField(
            model_name='salon',
            name='favoriting_customers',
            field=models.ManyToManyField(related_name='_favoriting_customers_+', through='favorites.FavoriteSalon', verbose_name='Favoriting Customers', blank=True, to='customers.Customer'),
        ),
    ]
