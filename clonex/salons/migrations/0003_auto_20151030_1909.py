# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('salons', '0002_salon_favoriting_customers'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salon',
            name='favoriting_customers',
            field=models.ManyToManyField(blank=True, to='customers.Customer', through='favorites.FavoriteSalon', verbose_name='Favoriting Customers'),
        ),
    ]
