# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields
import phonenumber_field.modelfields
import geoposition.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Salon',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='Is Active')),
                ('street', models.CharField(max_length=128, verbose_name='Street')),
                ('locality', models.CharField(max_length=128, verbose_name='Locality')),
                ('city', models.CharField(max_length=32, verbose_name='City', choices=[('North Cyprus', (('XXX_NICOSIA', 'Nicosia'), ('XXX_KYRENIA', 'Kyrenia'), ('XXX_FAMAGUSTA', 'Famagusta'), ('XXX_GUZELYURT', 'Guzelyurt'), ('XXX_ISKELE', 'Iskele')))])),
                ('country', models.CharField(max_length=3, verbose_name='Country', choices=[('XXX', 'North Cyprus')])),
                ('geoposition', geoposition.fields.GeopositionField(max_length=42, verbose_name='Geoposition')),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(max_length=128, verbose_name='Phone Number', blank=True)),
                ('website_url', models.URLField(verbose_name='Website URL', blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('modified_on', models.DateTimeField(auto_now=True, verbose_name='Modified On')),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description')),
                ('slug', autoslug.fields.AutoSlugField(populate_from='get_name_and_full_address', max_length=512, editable=False, verbose_name='Slug')),
            ],
            options={
                'verbose_name_plural': 'Salons',
                'verbose_name': 'Salon',
                'ordering': ('country', 'city', 'locality', 'name'),
            },
        ),
        migrations.CreateModel(
            name='SalonPicture',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='Created On')),
                ('modified_on', models.DateTimeField(auto_now=True, verbose_name='Modified On')),
                ('picture_height', models.PositiveIntegerField(default=0, verbose_name='Picture Height')),
                ('picture_width', models.PositiveIntegerField(default=0, verbose_name='Picture Width')),
                ('picture', models.ImageField(height_field='picture_height', max_length=256, width_field='picture_width', verbose_name='Picture', upload_to='salons')),
                ('is_primary', models.NullBooleanField()),
                ('salon', models.ForeignKey(related_query_name='salon_picture', verbose_name='Salon', related_name='salon_pictures', to='salons.Salon')),
            ],
            options={
                'verbose_name_plural': 'Salon Picture Relationships',
                'verbose_name': 'Salon Picture Relationship',
                'ordering': ('salon', '-is_primary', '-created_on'),
            },
        ),
        migrations.CreateModel(
            name='SalonWorkingDay',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('working_day', models.CharField(max_length=3, verbose_name='Working Day', choices=[('mon', 'Monday'), ('tue', 'Tuesday'), ('wed', 'Wednesday'), ('thu', 'Thursday'), ('fri', 'Friday'), ('sat', 'Saturday'), ('sun', 'Sunday')])),
                ('does_morning', models.BooleanField(default=False, verbose_name='Does Morning')),
                ('does_afternoon', models.BooleanField(default=False, verbose_name='Does Afternoon')),
                ('does_evening', models.BooleanField(default=False, verbose_name='Does Evening')),
                ('does_night', models.BooleanField(default=False, verbose_name='Does Night')),
                ('salon', models.ForeignKey(related_query_name='related_working_day', verbose_name='Salon', related_name='related_working_days', to='salons.Salon')),
            ],
            options={
                'verbose_name_plural': 'Salon Working Day Relationships',
                'verbose_name': 'Salon Working Day Relationship',
                'ordering': ('salon', 'working_day'),
            },
        ),
        migrations.CreateModel(
            name='SalonWorkingPeriod',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('working_day', models.CharField(max_length=3, verbose_name='Working Day', choices=[('mon', 'Monday'), ('tue', 'Tuesday'), ('wed', 'Wednesday'), ('thu', 'Thursday'), ('fri', 'Friday'), ('sat', 'Saturday'), ('sun', 'Sunday')])),
                ('working_from', models.TimeField(verbose_name='Working From')),
                ('working_to', models.TimeField(verbose_name='Working To')),
                ('salon', models.ForeignKey(related_query_name='related_working_period', verbose_name='Salon', related_name='related_working_periods', to='salons.Salon')),
            ],
            options={
                'verbose_name_plural': 'Salon Working Period Relationships',
                'verbose_name': 'Salon Working Period Relationship',
                'ordering': ('salon', 'working_day', 'working_from'),
            },
        ),
        migrations.AlterUniqueTogether(
            name='salonworkingday',
            unique_together=set([('salon', 'working_day')]),
        ),
        migrations.AlterUniqueTogether(
            name='salonpicture',
            unique_together=set([('salon', 'is_primary')]),
        ),
    ]
