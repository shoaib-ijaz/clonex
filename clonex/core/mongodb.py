import pymongo

from django.conf import settings


client = pymongo.MongoClient(settings.MONGO_URI)
mongodb = client.get_default_database()


class PageObjects():
    
    def __init__(self, has_previous, has_next, previous_page_number, next_page_number):
        self._has_previous = has_previous
        self._has_next = has_next
        self._previous_page_number = previous_page_number
        self._next_page_number = next_page_number

    def has_previous(self):
        return self._has_previous

    def has_next(self):
        return self._has_next

    def previous_page_number(self):
        return self._previous_page_number

    def next_page_number(self):
        return self._next_page_number


def paginate(objects, limit, page):
    try:
        # make limit value integer
        # if value is 0 make it 10
        limit = int(limit) or 10
    except:
        limit = 10

    try:
        # make page value integer
        # if value is 0 make it 1
        page = int(page) or 1
    except:
        page = 1

    count = objects.count()
    skip = limit * (page-1)

    if count > (skip+limit): 
        has_next = True
        next_page_number = page + 1
    else:
        has_next = False
        next_page_number = None
    
    page_objects = list(objects.skip(skip).limit(limit))
    page_objects_count = len(page_objects)

    if page == 1:
        has_previous = False
        previous_page_number = None
    else:
        has_previous = True
        previous_page_number = page - 1

    if page_objects_count == 0:
        x = count / limit
        y = count % limit
        if y > 0:
            page = x + 1
        else:
            page = 1

        skip = limit * (page-1)

        objects.rewind()
        page_objects = list(objects.skip(skip).limit(limit))

        has_previous = False
        previous_page_number = None

    return page_objects, PageObjects(has_previous, has_next, previous_page_number, next_page_number)