from django.db import models
from django.utils.translation import ugettext_lazy as _

from geoposition.fields import GeopositionField
from phonenumber_field.modelfields import PhoneNumberField

from .choices import Cities, Countries


class Activable(models.Model):
    is_active = models.BooleanField(default=True, 
        verbose_name=_("Is Active"))
    
    class Meta:
        abstract = True


class Archivable(models.Model):
    is_archived = models.NullBooleanField(
        verbose_name=_("Is Archived"))
    
    class Meta:
        abstract = True


class Addressable(models.Model):
    STREET_MAX_LENGTH = LOCALITY_MAX_LENGTH = 128

    street = models.CharField(max_length=STREET_MAX_LENGTH,
        verbose_name=_("Street"))
    locality = models.CharField(max_length=LOCALITY_MAX_LENGTH,
        verbose_name=_("Locality"))
    city = models.CharField(max_length=Cities.CHOICE_MAX_LENGTH, choices=Cities.CHOICES,
        verbose_name=_("City"))
    country = models.CharField(max_length=Countries.CHOICE_MAX_LENGTH, choices=Countries.CHOICES,
        verbose_name=_("Country"))
    geoposition = GeopositionField(
        verbose_name=_("Geoposition"))

    class Meta:
        abstract = True


class ContactInfoble(models.Model):
    phone_number = PhoneNumberField(blank=True,
        verbose_name=_("Phone Number"))
    website_url = models.URLField(blank=True,
        verbose_name=_("Website URL"))


    class Meta:
        abstract = True


class Timestampable(models.Model):
    created_on = models.DateTimeField(auto_now_add=True, 
        verbose_name=_("Created On"))
    modified_on = models.DateTimeField(auto_now=True, 
        verbose_name=_("Modified On"))
    
    class Meta:
        abstract = True