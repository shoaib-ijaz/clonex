from django.utils.translation import ugettext_lazy as _


# GENDER
# ------------------------------------------------------------------------------
class Genders:
    FEMALE = 'f'
    MALE = 'm'

    CHOICE_MAX_LENGTH = 1

    CHOICES = (
        (FEMALE, _('Female')),
        (MALE, _('Male')),
    )


# CITY
# ------------------------------------------------------------------------------
class Cities:
    CHOICE_MAX_LENGTH = 32

    CHOICES = (
        (_('North Cyprus'), (
            ('XXX_NICOSIA', _('Nicosia')),
            ('XXX_KYRENIA', _('Kyrenia')),
            ('XXX_FAMAGUSTA', _('Famagusta')),
            ('XXX_GUZELYURT', _('Guzelyurt')),
            ('XXX_ISKELE', _('Iskele')),
        )),
    )


# COUNTRY
# ------------------------------------------------------------------------------
class Countries:
    NORTH_CYPRUS = 'XXX'

    CHOICE_MAX_LENGTH = 3

    CHOICES = (
        (NORTH_CYPRUS, _('North Cyprus')),
    )


# CURRENCY
# ------------------------------------------------------------------------------
class Currencies:
    TURKISH_LIRA = 'TRY'

    CHOICE_MAX_LENGTH = 3

    CHOICES = (
        (TURKISH_LIRA, _('TRY (Turkish Lira)'))
)


# DAY
# ------------------------------------------------------------------------------
class Days:
    MONDAY = 'mon'
    TUESDAY = 'tue'
    WEDNESDAY = 'wed'
    THURSDAY = 'thu'
    FRIDAY = 'fri'
    SATURDAY = 'sat'
    SUNDAY = 'sun'

    CHOICE_MAX_LENGTH = 3

    CHOICES = (
        (MONDAY, _('Monday')),
        (TUESDAY, _('Tuesday')),
        (WEDNESDAY, _('Wednesday')),
        (THURSDAY, _('Thursday')),
        (FRIDAY, _('Friday')),
        (SATURDAY, _('Saturday')),
        (SUNDAY, _('Sunday')),
    )


# LANGUAGE
# ------------------------------------------------------------------------------
class Languages:
    CHOICE_MAX_LENGTH = 3


# TIMEZONE
# ------------------------------------------------------------------------------
class Timezones:
    CHOICE_MAX_LENGTH = 32

    CHOICES = (
        ('Europe/London', _('(+00:00) Europe/London')),
        ('Europe/Istanbul', _('(+02:00) Europe/Istanbul')),
        ('Europe/Nicosia', _('(+02:00) Europe/Nicosia')),
    )