from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from clonex.core.behaviors import Activable, Timestampable


class Customer(Activable, Timestampable, models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True, 
        related_query_name='customer',
        verbose_name=_("User"))


    # Favorites
    # -------------------------------------------------------------------
    favorited_professionals = models.ManyToManyField('professionals.Professional', blank=True,
        through='favorites.FavoriteProfessional',
        related_name='+',
        verbose_name=_("Favorited Professionals"))


    favorited_salons = models.ManyToManyField('salons.Salon', blank=True,
        through='favorites.FavoriteSalon',
        related_name='+',
        verbose_name=_("Favorited Salons"))


    # Recommendations
    # -------------------------------------------------------------------
    recommended_professionals = models.ManyToManyField('professionals.Professional',  blank=True,
        through='recommendations.ProfessionalRecommendation',
        related_name='+',
        verbose_name=_("Recommended Professionals"))


    recommended_salons = models.ManyToManyField('salons.Salon',  blank=True,
        through='recommendations.SalonRecommendation',
        related_name='+',
        verbose_name=_("Recommended Salons"))


    class Meta:
        verbose_name = _("Customer")
        verbose_name_plural = _("Customers")


    def __str__(self):
        return _("Customer [{pk}]: {full_name}").format(
            pk=self.pk, 
            full_name=self.user.get_full_name())


    def get_absolute_url(self):
        return reverse('customers:customer_detail', kwargs={'pk': self.pk})