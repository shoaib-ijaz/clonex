# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('favorites', '0001_initial'),
        ('professionals', '0001_initial'),
        ('customers', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='favorited_professionals',
            field=models.ManyToManyField(to='professionals.Professional', verbose_name='Favorited Professionals', through='favorites.FavoriteProfessional'),
        ),
    ]
