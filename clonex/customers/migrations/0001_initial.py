# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('is_active', models.BooleanField(verbose_name='Is Active', default=True)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('user', models.OneToOneField(related_query_name='customer', verbose_name='User', to=settings.AUTH_USER_MODEL, serialize=False, primary_key=True)),
            ],
            options={
                'verbose_name': 'Customer',
                'verbose_name_plural': 'Customers',
            },
        ),
    ]
