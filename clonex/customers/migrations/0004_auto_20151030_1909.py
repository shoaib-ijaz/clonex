# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_customer_favorited_salons'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='favorited_professionals',
            field=models.ManyToManyField(blank=True, to='professionals.Professional', through='favorites.FavoriteProfessional', verbose_name='Favorited Professionals'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='favorited_salons',
            field=models.ManyToManyField(blank=True, to='salons.Salon', through='favorites.FavoriteSalon', verbose_name='Favorited Salons'),
        ),
    ]
