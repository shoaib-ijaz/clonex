# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('professionals', '0004_auto_20151030_2218'),
        ('recommendations', '0001_initial'),
        ('salons', '0003_auto_20151030_1909'),
        ('customers', '0004_auto_20151030_1909'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='recommended_professionals',
            field=models.ManyToManyField(related_name='_recommended_professionals_+', through='recommendations.ProfessionalRecommendation', verbose_name='Recommended Professionals', blank=True, to='professionals.Professional'),
        ),
        migrations.AddField(
            model_name='customer',
            name='recommended_salons',
            field=models.ManyToManyField(related_name='_recommended_salons_+', through='recommendations.SalonRecommendation', verbose_name='Recommended Salons', blank=True, to='salons.Salon'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='favorited_professionals',
            field=models.ManyToManyField(related_name='_favorited_professionals_+', through='favorites.FavoriteProfessional', verbose_name='Favorited Professionals', blank=True, to='professionals.Professional'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='favorited_salons',
            field=models.ManyToManyField(related_name='_favorited_salons_+', through='favorites.FavoriteSalon', verbose_name='Favorited Salons', blank=True, to='salons.Salon'),
        ),
    ]
