# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('salons', '0002_salon_favoriting_customers'),
        ('favorites', '0001_initial'),
        ('customers', '0002_customer_favorited_professionals'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='favorited_salons',
            field=models.ManyToManyField(through='favorites.FavoriteSalon', to='salons.Salon', verbose_name='Favorited Salons'),
        ),
    ]
