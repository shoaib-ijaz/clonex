from django.db import models
from django.utils.translation import ugettext_lazy as _

from hvad.models import TranslatableModel, TranslatedFields

from clonex.core.behaviors import Timestampable


class ServiceCategory(Timestampable, TranslatableModel):

    translations = TranslatedFields(
        title=models.CharField(max_length=255, 
            verbose_name=_("Title")),
    )


    class Meta:
        verbose_name = _("Service Category")
        verbose_name_plural = _("Service Categories")


    def __str__(self, language=None):
        return _("Service Category [{id}]: {title}").format(
            id=self.id,
            title=self.title)



class Service(Timestampable, TranslatableModel):

    service_category = models.ForeignKey('services.ServiceCategory',
        related_name='services',
        related_query_name='service',
        verbose_name=_("Service Category"))
    
    translations = TranslatedFields(
        title=models.CharField(max_length=255,
            verbose_name=_("Title")),
        description=models.CharField(max_length=1024,
            verbose_name=_("Description"))
    )

    duration = models.DurationField(
        verbose_name=_("Duration"))


    class Meta:
        ordering = ('service_category',)
        verbose_name = _("Service")
        verbose_name_plural = _("Services")


    def __str__(self, language=None):
        return _("Service [{id}]: {title}").format(
            id=self.id,
            title=self.title)


class ProfessionalService(Timestampable, TranslatableModel):

    professional = models.ForeignKey('professionals.Professional',
        related_name='services',
        related_query_name='service',
        verbose_name=_("Professional"))

    service = models.ForeignKey('services.Service',
        related_name='services',
        related_query_name='service',
        verbose_name=_("Service"))

    translations = TranslatedFields(
        description=models.CharField(max_length=1024,
            verbose_name=_("Description"))
    )

    duration = models.DurationField(
        verbose_name=_("Duration"))
    price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True,
        verbose_name=_("Price"))
    is_base_price = models.NullBooleanField(
        verbose_name=_("Is Base Price"))


    class Meta:
        ordering = ('professional', 'service__service_category',)
        verbose_name = _("Professional Service Relationship")
        verbose_name_plural = _("Professional Service Relationships")


    def __str__(self):
        return _("Professional Service Relationship [{id}]: {professional} {service}").format(
            id=self.id,
            professional=self.professional,
            service=self.service)