# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('professionals', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfessionalService',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('duration', models.DurationField(verbose_name='Duration')),
                ('price', models.DecimalField(null=True, verbose_name='Price', decimal_places=2, blank=True, max_digits=8)),
                ('is_base_price', models.NullBooleanField(verbose_name='Is Base Price')),
                ('professional', models.ForeignKey(related_query_name='service', related_name='services', verbose_name='Service Category', to='professionals.Professional')),
            ],
            options={
                'verbose_name': 'Professional Service Relationship',
                'ordering': ('professional', 'service__service_category'),
                'verbose_name_plural': 'Professional Service Relationships',
            },
        ),
        migrations.CreateModel(
            name='ProfessionalServiceTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('description', models.CharField(max_length=1024, verbose_name='Description')),
                ('language_code', models.CharField(max_length=15, db_index=True)),
                ('master', models.ForeignKey(null=True, to='services.ProfessionalService', related_name='translations', editable=False)),
            ],
            options={
                'managed': True,
                'db_tablespace': '',
                'db_table': 'services_professionalservice_translation',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('duration', models.DurationField(verbose_name='Duration')),
            ],
            options={
                'verbose_name': 'Service',
                'ordering': ('service_category',),
                'verbose_name_plural': 'Services',
            },
        ),
        migrations.CreateModel(
            name='ServiceCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('created_on', models.DateTimeField(verbose_name='Created On', auto_now_add=True)),
                ('modified_on', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
            ],
            options={
                'verbose_name': 'Service Category',
                'verbose_name_plural': 'Service Categories',
            },
        ),
        migrations.CreateModel(
            name='ServiceCategoryTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('language_code', models.CharField(max_length=15, db_index=True)),
                ('master', models.ForeignKey(null=True, to='services.ServiceCategory', related_name='translations', editable=False)),
            ],
            options={
                'managed': True,
                'db_tablespace': '',
                'db_table': 'services_servicecategory_translation',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ServiceTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('description', models.CharField(max_length=1024, verbose_name='Description')),
                ('language_code', models.CharField(max_length=15, db_index=True)),
                ('master', models.ForeignKey(null=True, to='services.Service', related_name='translations', editable=False)),
            ],
            options={
                'managed': True,
                'db_tablespace': '',
                'db_table': 'services_service_translation',
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='service',
            name='service_category',
            field=models.ForeignKey(related_query_name='service', related_name='services', verbose_name='Service Category', to='services.ServiceCategory'),
        ),
        migrations.AddField(
            model_name='professionalservice',
            name='service',
            field=models.ForeignKey(related_query_name='service', related_name='services', verbose_name='Service Category', to='services.Service'),
        ),
        migrations.AlterUniqueTogether(
            name='servicetranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.AlterUniqueTogether(
            name='servicecategorytranslation',
            unique_together=set([('language_code', 'master')]),
        ),
        migrations.AlterUniqueTogether(
            name='professionalservicetranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
