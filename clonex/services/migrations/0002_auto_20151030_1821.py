# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='professionalservice',
            name='professional',
            field=models.ForeignKey(verbose_name='Professional', to='professionals.Professional', related_query_name='service', related_name='services'),
        ),
        migrations.AlterField(
            model_name='professionalservice',
            name='service',
            field=models.ForeignKey(verbose_name='Service', to='services.Service', related_query_name='service', related_name='services'),
        ),
    ]
