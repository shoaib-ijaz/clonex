from django.contrib import admin

from hvad.admin import TranslatableAdmin

from .models import ServiceCategory, Service, ProfessionalService


class ServiceCategoryAdmin(TranslatableAdmin):
	pass


class ServiceAdmin(TranslatableAdmin):
	pass


class ProfessionalServiceAdmin(TranslatableAdmin):
	pass


admin.site.register(ServiceCategory, ServiceCategoryAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(ProfessionalService, ProfessionalServiceAdmin)