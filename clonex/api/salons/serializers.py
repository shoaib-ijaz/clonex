from rest_framework import serializers

from clonex.salons.models import Salon


class SalonSerializer(serializers.ModelSerializer):

    class Meta:
        model = Salon
        fields = (
			# system
        	'id', 'slug', 'created_on', 'modified_on',
        	# basic
        	'name', 'description',
        	# address
        	'street',
        	'locality',
        	'city',
        	'country',
        	'geoposition',
        	# contact info
        	'phone_number',
        	'website_url',
        )