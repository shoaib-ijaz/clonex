from rest_framework import filters, permissions, viewsets

from clonex.salons.models import Salon

from .serializers import SalonSerializer


class SalonViewSet(viewsets.ModelViewSet):
    queryset = Salon.objects.all()
    serializer_class = SalonSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('name', 'street', 'locality', 'city', 'country', 'salon__name',)