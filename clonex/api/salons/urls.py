from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = format_suffix_patterns([
    url(r'^$',
        views.SalonList.as_view(),
        name='salon-list'),
    url(r'^(?P<pk>[0-9]+)/$',
        views.SalonDetail.as_view(),
        name='salon-detail'),
])