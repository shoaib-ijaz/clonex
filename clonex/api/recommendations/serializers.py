from rest_framework import serializers

from clonex.recommendations.models import ProfessionalRecommendation, SalonRecommendation


class ProfessionalRecommendationSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProfessionalRecommendation
        fields = ('id', 'customer', 'professional', 'recommendation',)


class SalonRecommendationSerializer(serializers.ModelSerializer):

    class Meta:
        model = SalonRecommendation
        fields = ('id', 'customer', 'salon', 'recommendation',)