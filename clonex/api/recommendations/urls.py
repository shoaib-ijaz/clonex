from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = format_suffix_patterns([
    url(r'^professional-recommendations/$',
        views.ProfessionalRecommendationList.as_view(),
        name='professional-recommendation-list'),
    url(r'^professional-recommendations/(?P<pk>[0-9]+)/$',
        views.ProfessionalRecommendationDetail.as_view(),
        name='professional-recommendation-detail'),
    url(r'^salon-recommendations/$',
        views.SalonRecommendationList.as_view(),
        name='salon-recommendation-list'),
    url(r'^salon-recommendations/(?P<pk>[0-9]+)/$',
        views.SalonRecommendationDetail.as_view(),
        name='salon-recommendation-detail'),
])