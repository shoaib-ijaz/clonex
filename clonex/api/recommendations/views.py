from rest_framework import filters, permissions, viewsets

from clonex.recommendations.models import ProfessionalRecommendation, SalonRecommendation

from .serializers import ProfessionalRecommendationSerializer, SalonRecommendationSerializer


class ProfessionalRecommendationViewSet(viewsets.ModelViewSet):
    queryset = ProfessionalRecommendation.objects.all()
    serializer_class = ProfessionalRecommendationSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('customer', 'professional', 'professional__salon',)


class SalonRecommendationViewSet(viewsets.ModelViewSet):
    queryset = SalonRecommendation.objects.all()
    serializer_class = SalonRecommendationSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('customer', 'salon',)