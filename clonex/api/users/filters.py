

class UserFilter(filters.FilterSet):
    username = filters.AllLookupsFilter(name='username')
    first_name = filters.AllLookupsFilter(name='first_name')
    last_name = filters.AllLookupsFilter(name='last_name')
    birthday = filters.AllLookupsFilter(name='birthday')
    gender = filters.AllLookupsFilter(name='gender')

    class Meta:
        model = Organization
        fields = (
            'company',
            'description', 'contact_name', 'contact_phone',
            'contact_email',
        )