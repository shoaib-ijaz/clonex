from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = format_suffix_patterns([
    url(r'^services/$',
        views.ServiceList.as_view(),
        name='service-list'),
    url(r'^services/(?P<pk>[0-9]+)/$',
        views.ServiceDetail.as_view(),
        name='service-detail'),
    url(r'^professional-service-relationships/$',
        views.ProfessionalServiceList.as_view(),
        name='professional-service-list'),
    url(r'^professional-service-relationships/(?P<pk>[0-9]+)/$',
        views.ProfessionalServiceDetail.as_view(),
        name='professional-service-detail'),
])