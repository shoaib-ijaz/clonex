from hvad.contrib.restframework import TranslationsMixin
from rest_framework import serializers

from clonex.services.models import Service, ProfessionalService


class ServiceSerializer(TranslationsMixin, serializers.ModelSerializer):

    class Meta:
        model = Service
        fields = ('id', 'service_category', 'translations', 'duration',)


class ProfessionalServiceSerializer(TranslationsMixin, serializers.ModelSerializer):

    class Meta:
        model = ProfessionalService
        fields = ('id', 'professional', 'service', 'translations', 'duration', 'price', 'is_base_price',)