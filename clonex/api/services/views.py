from rest_framework import filters, permissions, viewsets

from clonex.services.models import Service, ProfessionalService

from .serializers import ServiceSerializer, ProfessionalServiceSerializer


class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('service_category',)


class ProfessionalServiceViewSet(viewsets.ModelViewSet):
    queryset = ProfessionalService.objects.all()
    serializer_class = ProfessionalServiceSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('professional', 'service', 'duration', 'price', 'is_base_price',)