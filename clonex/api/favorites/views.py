from rest_framework import filters, permissions, viewsets

from clonex.favorites.models import FavoriteProfessional, FavoriteSalon

from .serializers import FavoriteProfessionalSerializer, FavoriteSalonSerializer


class FavoriteProfessionalViewSet(viewsets.ModelViewSet):
    queryset = FavoriteProfessional.objects.all()
    serializer_class = FavoriteProfessionalSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('customer', 'professional',)


class FavoriteSalonViewSet(viewsets.ModelViewSet):
    queryset = FavoriteSalon.objects.all()
    serializer_class = FavoriteSalonSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('customer', 'salon',)