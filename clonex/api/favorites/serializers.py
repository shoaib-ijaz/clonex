from rest_framework import serializers

from clonex.favorites.models import FavoriteProfessional, FavoriteSalon


class FavoriteProfessionalSerializer(serializers.ModelSerializer):

    class Meta:
        model = FavoriteProfessional
        fields = ('id', 'customer', 'professional',)


class FavoriteSalonSerializer(serializers.ModelSerializer):

    class Meta:
        model = FavoriteSalon
        fields = ('id', 'customer', 'salon',)