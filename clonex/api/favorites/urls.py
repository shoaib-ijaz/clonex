from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = format_suffix_patterns([
    url(r'^favorite-professional-relationships/$',
        views.FavoriteProfessionalList.as_view(),
        name='favorite-professional-list'),
    url(r'^favorite-professional-relationships/(?P<pk>[0-9]+)/$',
        views.FavoriteProfessionalDetail.as_view(),
        name='favorite-professional-detail'),
    url(r'^favorite-salon-relationships/$',
        views.FavoriteSalonList.as_view(),
        name='favorite-salon-list'),
    url(r'^favorite-salon-relationships/(?P<pk>[0-9]+)/$',
        views.FavoriteSalonDetail.as_view(),
        name='favorite-salon-detail'),
])