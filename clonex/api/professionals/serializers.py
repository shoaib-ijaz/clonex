from rest_framework import serializers

from clonex.professionals.models import Professional
from clonex.salons.models import Salon

from clonex.api.salons.serializers import SalonSerializer
from clonex.api.users.serializers import UserSerializer


class ProfessionalSerializer(serializers.ModelSerializer):
    user = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='api:user-detail',
    )
    salon = serializers.HyperlinkedRelatedField(
        queryset=Salon.objects.all(),
        view_name='api:salon-detail',
    )

    class Meta:
        model = Professional
        fields = (
        	# system
        	'user', 'slug', 'created_on', 'modified_on',
        	# basic
        	'name', 'description',
        	# salon
        	'salon',
        	# booking
        	'does_online_booking',
        	# address
        	'street',
        	'locality',
        	'city',
        	'country',
        	'geoposition',
        	# contact info
        	'phone_number',
        	'website_url',
        )