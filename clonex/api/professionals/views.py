from rest_framework import filters, permissions, viewsets

from clonex.professionals.models import Professional

from .serializers import ProfessionalSerializer


class ProfessionalViewSet(viewsets.ModelViewSet):
    queryset = Professional.objects.all()
    serializer_class = ProfessionalSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('user__username', 'user__first_name', 'user__last_name', 'name', 'street', 'locality', 'city', 'country', 'salon', 'salon__name',)