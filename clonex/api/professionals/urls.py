from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = format_suffix_patterns([
    url(r'^$',
        views.ProfessionalList.as_view(),
        name='professional-list'),
    url(r'^(?P<pk>[0-9]+)/$',
        views.ProfessionalDetail.as_view(),
        name='professional-detail'),
])