from rest_framework import filters, permissions, viewsets

from clonex.customers.models import Customer

from .serializers import CustomerSerializer


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = (
    	# joining user model
    	'user__username', 'user__first_name', 'user__last_name', 'user__birthday', 'user__gender', 'user__language', 'user__timezone',)