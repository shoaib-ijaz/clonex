from rest_framework import serializers

from clonex.api.users.serializers import UserSerializer

from clonex.customers.models import Customer



class CustomerSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Customer
        fields = ('user',)