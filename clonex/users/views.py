# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from braces.views import LoginRequiredMixin

from .models import User


class UserListView(ListView):
    model = User
    context_object_name = 'user_list'
    template_name = 'users/user_list.html'


class UserDetailRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('users:user_detail', 
            kwargs={'username': self.request.user.username})


class UserDetailView(DetailView):
    model = User
    context_object_name = 'user'
    template_name = 'users/user_detail.html'
    # These next two lines tell the view to index lookups by username
    slug_field = "username"
    slug_url_kwarg = "username"


class UserUpdateView(LoginRequiredMixin, UpdateView):

    fields = ['first_name', 'last_name', 'birthday', 'gender', 'language', 'timezone',]

    # we already imported User in the view code above, remember?
    model = User

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse("users:user_detail",
                       kwargs={"username": self.request.user.username})

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(username=self.request.user.username)