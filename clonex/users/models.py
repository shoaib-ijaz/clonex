# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from clonex.core.choices import Genders, Languages, Timezones

from clonex.customers.models import Customer


class User(AbstractUser):
    LANGUAGE_MAX_LENGTH = 5
    TIMEZONE_MAX_LENGTH = 32

    birthday = models.DateField(blank=True, null=True, 
        verbose_name=_("Birthday"))
    gender = models.CharField(max_length=Genders.CHOICE_MAX_LENGTH, choices=Genders.CHOICES, blank=True,
        verbose_name=_("Gender"))

    language = models.CharField(max_length=Languages.CHOICE_MAX_LENGTH, choices=settings.LANGUAGES, default=settings.LANGUAGE_CODE, 
        verbose_name=_("Language"))
    timezone = models.CharField(max_length=Timezones.CHOICE_MAX_LENGTH, choices=Timezones.CHOICES, default=settings.TIME_ZONE, 
        verbose_name=_("Timezone"))


    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")


    def __str__(self):
        return _("User [{id}]: {username} ({full_name})").format(
            id=self.id, 
            username=self.username,
            full_name=self.get_full_name())

    def get_absolute_url(self):
        return reverse('users:user_detail', kwargs={'username': self.username})


@receiver([models.signals.post_save], sender=settings.AUTH_USER_MODEL)
def create_customer(sender, instance, created, **kwargs):
    if created:
        Customer.objects.create(user=instance)