# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    
    # URL pattern for the UserListView
    url(
        regex=r'^$',
        view=views.UserListView.as_view(),
        name='user_list'
    ),
    # URL pattern for the UserRedirectView
    url(
        regex=r'^me/$',
        view=views.UserDetailRedirectView.as_view(),
        name='user_detail_redirect'
    ),
    # URL pattern for the UserDetailView
    url(
        regex=r'^(?P<username>[\w.@+-]+)/$',
        view=views.UserDetailView.as_view(),
        name='user_detail'
    ),
    # URL pattern for the UserUpdateView
    url(
        regex=r'^me/~update/$',
        view=views.UserUpdateView.as_view(),
        name='user_update'
    ),
]