# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='pages/home.html'), name="home"),
    url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'), name="about"),

    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),

    # User management
    url(r'^accounts/', include('allauth.urls')),
    url(r'^users/', include("clonex.users.urls", namespace="users")),
    # rest_framework browsable api login
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),


] 

# API
# ----------------------------------------------------------------------

from rest_framework import routers

from clonex.api.customers.views import CustomerViewSet
from clonex.api.favorites.views import FavoriteProfessionalViewSet, FavoriteSalonViewSet
from clonex.api.professionals.views import ProfessionalViewSet
from clonex.api.recommendations.views import ProfessionalRecommendationViewSet, SalonRecommendationViewSet
from clonex.api.salons.views import SalonViewSet
from clonex.api.services.views import ServiceViewSet, ProfessionalServiceViewSet
from clonex.api.users.views import UserViewSet


router = routers.SimpleRouter()

router.register(r'customers', CustomerViewSet, 
    base_name='Customer')
router.register(r'favorite-professional-relationships', FavoriteProfessionalViewSet, 
    base_name='FavoriteProfessional')
router.register(r'favorite-salon-relationships', FavoriteSalonViewSet, 
    base_name='FavoriteSalon')
router.register(r'professionals', ProfessionalViewSet, 
    base_name='Professional')
router.register(r'professional-recommendations', ProfessionalRecommendationViewSet, 
    base_name='ProfessionalRecommendation')
router.register(r'salon-recommendations', SalonRecommendationViewSet, 
    base_name='SalonRecommendation')
router.register(r'salons', SalonViewSet, 
    base_name='Salon')
router.register(r'services', ServiceViewSet, 
    base_name='Service')
router.register(r'professional-service-relationships', ProfessionalServiceViewSet, 
    base_name='ProfessionalService')
router.register(r'users', UserViewSet, 
    base_name='User')

urlpatterns += [
    url(r'^api/', include(router.urls, namespace="api")),
]

# MEDIA
# ----------------------------------------------------------------------

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# DEBUGGER
# ----------------------------------------------------------------------

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request),
        url(r'^403/$', default_views.permission_denied),
        url(r'^404/$', default_views.page_not_found),
        url(r'^500/$', default_views.server_error),
    ]